export class Joueur{
    name: string;
    score: number;

    constructor(name: string, score: number){
        this.name = name;
        this.score = score;
    }
  }