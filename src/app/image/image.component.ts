import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit, OnChanges {

  mot = 'test';

  color = '';

  @Input() image: number;

  showImage = false;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(change){
    if (this.image >= 7) {
      this.showImage = true;
      this.color = 'red';
    } else {
      this.showImage = false;
      this.color = 'green';
    }
  }
}
