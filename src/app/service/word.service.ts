import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WordService {

  word = ['test'];

  tabWord = [];

  constructor() { }

  checkWord(lettre: string){
    // Vérifie si la sous-chaîne existe dans la chaîne de caractères
    if (this.word[0].includes(lettre)){
        alert('La sous-chaîne existe!');
        return true;
    } else {
        alert('La lettre n\'existe pas!');
        return false;
    }
  }

  transformTabWord(){
    this.word[0] = 'test';
    this.tabWord = this.word[0].split('');
    return this.tabWord;
  }

  getWord(){
    console.log(this.word[0]);
    return this.word[0];
  }
}
