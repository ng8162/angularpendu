import { Component, OnInit } from '@angular/core';
import { Joueur } from '../Models/Joueur';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {

  joueur1 = new Joueur('Steven', 18);
  joueur2 = new Joueur('Julie', 15);
  joueur3 = new Joueur('Quentin', 14);

  joueurs = [];

  constructor() { }

  ngOnInit(): void {
    this.joueurs = [this.joueur1, this.joueur2, this.joueur3];
  }

}
