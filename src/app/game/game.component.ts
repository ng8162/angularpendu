import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  constructor(private router: Router) { }

  image = '';

  ngOnInit(): void {
  }

  sendImageToImageComponent(image){
    console.log('image app-component', image);
    this.image = image;
  }

  sendWinToFatherToImageComponent(image){
    this.image = image;
  }

  init(){
    location.reload();
  }

}
