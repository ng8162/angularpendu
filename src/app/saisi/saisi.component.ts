import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WordService } from '../service/word.service';

@Component({
  selector: 'app-saisi',
  templateUrl: './saisi.component.html',
  styleUrls: ['./saisi.component.css']
})
export class SaisiComponent implements OnInit {

  saisi = '';

  check: boolean;

  image = 0;

  tabWord = [];

  letterShow = true;

  LetterFind = [];

  saisiWord = '';

  divWord;

  @Output() sendImageToFather = new EventEmitter();

  @Output() sendWinToFather = new EventEmitter();

  constructor(private wordService: WordService) { }

  ngOnInit(): void {
    this.tabWord = this.wordService.transformTabWord();
  }

  checkWord(){
    this.check = this.wordService.checkWord(this.saisi);
    if (this.check === true) {
      this.LetterFind = [this.saisi];
    } else {
      this.image++;
      console.log('image saisi-component', this.image);
      this.sendImageToFather.emit(this.image);
    }
    this.divWord  = document.getElementById('word');
    this.tabWord.forEach(element => {
      this.LetterFind.forEach(element2 => {
        if (element === element2) {
          this.divWord.innerHTML = this.divWord.innerHTML + "<span>"+ element +"<\span>" 
        } else {
          this.divWord.innerHTML = this.divWord.innerHTML + "<span> _ <\span>";
        }
      });
    });
    this.divWord.innerHTML = this.divWord.innerHTML + '<br>';

    if (this.LetterFind.length === this.tabWord.length) {
      alert('vous avez trouver le mot');
    }
  }

  checkWordSaisi(){
    if (this.saisiWord === this.wordService.getWord()) {
      this.sendWinToFather.emit('win');

    }else{
      this.image++;
      this.sendImageToFather.emit(this.image);
    }
  }
}
